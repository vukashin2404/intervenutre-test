import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

export const routes: Routes = [
  { path: 'products', loadChildren: '@app/features/products/products.module#ProductsModule'},
  { path: 'stock', loadChildren: '@app/features/stock-inventory/stock-inventory.module#StockInventoryModule'},
  { path: '', redirectTo: '/stock', pathMatch: 'full' },
  /*********NotFound*******/
  { path: '**', redirectTo: '/products' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      useHash: false,
      scrollPositionRestoration: 'enabled',
      // anchorScrolling: 'enabled',
      preloadingStrategy: PreloadAllModules
    })
  ],
  exports: [
    RouterModule
  ]
})

export class AppRoutingModule {
}
