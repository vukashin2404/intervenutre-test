import { appear } from './appear.animations';
import { fadeOut } from './fade-out.animations';

export const animation: any[] = [
    fadeOut,
    appear
  ];

export { appear } from './appear.animations';
export { fadeOut } from './fade-out.animations';
