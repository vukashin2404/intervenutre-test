
import { trigger, state, style, animate, transition } from '@angular/animations';

export const appear = trigger('Appear', [
  state('appear', style({ transform : 'scale(1)', opacity : 1})),
  transition(':enter', [
    style({ transform: 'scale(4)', opacity : 0 }),
    animate('600ms ease-in-out')
  ]),
]);
