
import { trigger, state, style, animate, transition } from '@angular/animations';

export const fadeOut = trigger('Leave', [
  state('fadeOut', style({ opacity : 1})),
  transition(':leave', animate('0.3s ease-out', style({ opacity : 0}))),
]);
