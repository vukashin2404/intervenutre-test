import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MaterialModule } from '@app/shared/modules/material.module';


// modules
const modules: any[] = [
  CommonModule,
  HttpClientModule,
  MaterialModule,
  FormsModule,
  ReactiveFormsModule
];

/*******pipes*******/
import * as fromComponents from './components';

/*******pipes*******/
import * as fromPipes from './pipes';


@NgModule({
  imports: [
    ...modules,
  ],
  declarations: [
    ...fromPipes.pipes,
    ...fromComponents.components
  ],
  exports : [
    ...modules,
    ...fromPipes.pipes,
    ...fromComponents.components
  ]

})
export class SharedModule { }
