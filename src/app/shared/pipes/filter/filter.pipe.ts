import { Pipe, PipeTransform } from '@angular/core';


@Pipe({ name: 'mkpFilter' })
export class FilterPipe implements PipeTransform {
  transform(items: any[], term: string, val: boolean): any {

      return items.filter(item => {
console.log( item[term]);
        return item[term] === val;
      });
  }
}
