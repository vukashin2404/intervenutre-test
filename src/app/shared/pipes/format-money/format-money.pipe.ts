import { Pipe, PipeTransform } from '@angular/core';
import { formatCurrency } from '@angular/common';

@Pipe({
    name: 'formatMoney'
})
export class FormatMoneyPipe implements PipeTransform {
    transform(value: any): any {
        return formatCurrency(value, 'en', '€ ', 'EUR', '1.2-2');
    }
}

