
import { EllipsisPipe } from './ellipsis/ellipsis.pipe';
import { FilterPipe } from './filter/filter.pipe';
import { FormatMoneyPipe } from './format-money/format-money.pipe';


export const pipes: any[] = [
    EllipsisPipe,
    FilterPipe,
    FormatMoneyPipe
];

export * from './ellipsis/ellipsis.pipe';
export * from './filter/filter.pipe';
