import { Component } from '@angular/core';

@Component({
selector: 'app-spinner',
template: `
        <mat-spinner [diameter]="25"></mat-spinner>
    `,
    styles : [`

    `]
    })

export class LoaderComponent  {

}


