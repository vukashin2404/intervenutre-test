export class NotificationModel {
    public message : string;
    public action? : string;
    public duration? : number;

    constructor (
        message : string,
        action? : string,
        duration? : number
        ) {
            this.message  = message;
            this.action  = action;
            this.duration  = duration;
    }
}
