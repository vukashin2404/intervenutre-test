import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})

export class UtilsHelpers {
    /* immutable state */
    updateObject(oldObject, updatedProperties) {
            return {
                ...oldObject,
                ...updatedProperties
            };
        }

        /* flatten and turn array in object */
    flatten(arr: any[]) {
        const entities = arr.reduce((acc, item) => ({ ...acc, [item.id]: item }), {});
        return entities;
    }

}
