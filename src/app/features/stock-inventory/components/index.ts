import { StockInventoryComponent } from './stock-inventory/stock-inventory.component';
import { StockBranchComponent } from './stock-branch/stock-branch.component';
import { StockSelectorComponent } from './stock-selector/stock-selector.component';
import { StockProductsComponent } from './stock-products/stock-products.component';
import { StockCounterComponent } from './stock-counter/stock-counter.component';

export const components: any[] = [
  StockInventoryComponent,
  StockBranchComponent,
  StockSelectorComponent,
  StockProductsComponent,
  StockCounterComponent
  ];


export { StockInventoryComponent } from './stock-inventory/stock-inventory.component';
export { StockBranchComponent } from './stock-branch/stock-branch.component';
export { StockSelectorComponent } from './stock-selector/stock-selector.component';
export { StockProductsComponent } from './stock-products/stock-products.component';
export { StockCounterComponent } from './stock-counter/stock-counter.component';


