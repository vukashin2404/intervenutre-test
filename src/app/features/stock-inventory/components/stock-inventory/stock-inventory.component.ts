

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray, AbstractControl} from '@angular/forms';

import { StockInventoryService} from '@app/features/stock-inventory/services/stock-inventory.service';

import { StockValidators} from '@app/features/stock-inventory/validators/stock-inventory.validators';


/********rxjs*******/
import {forkJoin} from 'rxjs';
import { map } from 'rxjs/operators';

/* model */
import { Product } from '@app/features/stock-inventory/models/product.interface';

@Component({
  selector: 'app-stock-inventory',
  templateUrl: 'stock-inventory.component.html'
})

export class StockInventoryComponent implements OnInit {

    products: Product[];
    total: Number;

    // { 1 : Product }
    productMap: Map<number, Product>;

    form = this.fb.group({
        // Manage who
        store : this.fb.group({
            branch : ['', [Validators.required , StockValidators.checkBranch], [this.validateBranch.bind(this)]],
            code : ['', Validators.required]
            }),
        // Manage what
        selector : this.createStock({}),
        // Collection
        stock : this.fb.array([])
        }, { validator: StockValidators.checkStockExists});

    constructor(
        private fb: FormBuilder,
        private _stockService: StockInventoryService) {}


    ngOnInit() {
        const cart = this._stockService.getStocks();
        const products =  this._stockService.getProducts();


        forkJoin(cart, products)
            .subscribe(([cartItem, productItems]: [any[], Product[]]) => {
                // create new instance of the map
                // iterate over the products
                const myMap = productItems.map<[number, Product]>(product => [product.id, product]);
                this.productMap = new Map<number, Product>(myMap);
                this.products = productItems;

                cartItem.forEach(item => this.addStock(item));

                this.calculateTotal(this.form.get('stock').value);
                // calculate total
                this.form.get('stock')
                    .valueChanges.subscribe(value => this.calculateTotal(value));
            });
        }

        validateBranch(control: AbstractControl) {

            return this._stockService
                .checkBranchId(control.value)
                .pipe(
                    map((response: boolean) => {
                        return response ? null : { unknownBranch : true};
                    })
                );
        }

        calculateTotal(value: any[]) {
            const total = value.reduce((prev, next) => {
                // convert to number
                const price = +this.productMap.get(next.id).price;
                return prev + (next.quantity * price);
            }, 0);

            this.total = total;
        }

        createStock(stock) {
            return this.fb.group({
                id : parseInt(stock.id, 10) || '',
                quantity : stock.quantity || 10
            });
        }

        addStock(stock) {
            const control = this.form.get('stock') as FormArray;
            control.push(this.createStock(stock));
        }

        removeStock({group, index}: { group: FormGroup, index: number} ) {
            const control = this.form.get('stock') as FormArray;
            control.removeAt(index);

        }

        onSubmit() {
            console.log('submit:', this.form.value);
        }
 }

