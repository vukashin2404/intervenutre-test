

import { Component, Input, EventEmitter, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';

/* model */
import { Product } from '@app/features/stock-inventory/models/product.interface';


@Component({
  selector: 'app-stock-selector',
  template: `
  <div class="stock-selector" [formGroup]="parent">
      <div formGroupName="selector">
        <select formControlName="id">
          <option value="">Select stock</option>
          <option
            *ngFor="let product of products"
            [value]="product.id">
            {{ product.name }}
          </option>
        </select>
        <app-stock-counter
          [step]="10"
          [min]="10"
          [max]="1000"
          formControlName="quantity">
        </app-stock-counter>
        <button
          type="button"
          [disabled]="stockExists || notSelected"
          (click)="onAdd()">
          Add stock
        </button>
        <div
          class="stock-selector__error"
          *ngIf="stockExists">
          Item already exists in the stock
        </div>
      </div>
    </div>
  `,
  styleUrls : ['stock-selector.component.scss']
})

export class StockSelectorComponent  {
    @Input() parent: FormGroup;
    @Input() products: Product[];

    @Output() added = new EventEmitter<any>();

    get notSelected() {
        return (
            !this.parent.get('selector.id').value
        );
    }
    get stockExists() {
        return (
            this.parent.hasError ('stockExists')
            && this.parent.get('selector.id').dirty
        );
    }

    onAdd() {
        this.added.emit(this.parent.get('selector').value);
        /*
        // Update just one value
        this.parent.get('selector').patchValue({
            product_id : ''
        });

        or
            // need to provide the key and value
        this.parent.get('selector').setValue({
            product_id : '',
            quantity : 10
        });

        */
        this.parent.get('selector').reset({
            id : '',
            quantity : 10
        });
    }
 }

