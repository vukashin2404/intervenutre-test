import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '@app/shared/modules/material.module';

/***** modules ****/
import { SharedModule } from '@app/shared/shared.module';

/*****Components****/
import * as fromComponents from './components';

/*****Components****/
import * as fromPages from './pages';

/***** routing ****/
import { StockInventoryRoutingModule } from './stock-inventory-routing.module';


@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    SharedModule,
    StockInventoryRoutingModule
  ],
  declarations: [
    ...fromComponents.components,
    ...fromPages.pages
  ],
  exports: [
    ...fromComponents.components,
    ...fromPages.pages
  ]
})
export class StockInventoryModule { }


