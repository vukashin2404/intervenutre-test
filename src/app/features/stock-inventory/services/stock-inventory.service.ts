import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

/********rxjs*******/
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';

/*********** Import Model *********/
import { Product } from '@app/features/stock-inventory/models/product.interface';

/*********** Import Resource service *********/
import { ResourceService } from '@app/shared/services/resources.service';

/*********** Import CompanySerializer interface *********/
import { ProductSerializer } from '@app/features/products/interfaces/product-serializer.interface';

@Injectable({
  providedIn: 'root'
})

export class StockInventoryService extends ResourceService<Product> {

    constructor(httpClient: HttpClient) {
        super(
            httpClient,
            'http://localhost:3000',
            'procucts',
            new ProductSerializer()
        );
    }

    getStocks(): Observable<any[]> {

        return this.httpClient
              .get<any>(`http://localhost:3000/products`)
              .pipe(
                map((data: any) => data)
              );
      }

    getProducts(): Observable<Product[]> {

    return this.httpClient
            .get<any>(`http://localhost:3000/newproducts`)
            .pipe(
            map((data: any) => data)
            );
    }

    checkBranchId(id: string): Observable<boolean> {
        const params = new HttpParams().set('id', id);

        return this.httpClient
                .get<any>(`http://localhost:3000/branches`, { params })
                .pipe(
                map((data: any) => data),
                map((response: any[]) => {
                    return !!response.length;
                })
                );
        }

}


