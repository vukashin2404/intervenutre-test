/*
 ProductSerializer
*/


/*********** Import Model *********/
import { Product } from '@app/features/products/models/product.model';

export class ProductSerializer {
    fromJson(json: any): Product {
      const product = new Product(
        json.id,
        json.index,
        json.isActive,
        json.like,
        json.price,
        json.quantity,
        json.share,
        json.picture,
        json.name,
        json.description
      );

      return product;
    }

    toJson(product: Product): any {
      return {
        id : product.id ,
        index : product.index,
        isActive : product.isActive,
        like : product.like,
        price : product.price,
        quantity : product.quantity,
        share : product.share,
        picture : product.picture,
        name : product.name,
        description : product.description
      };
    }
  }


