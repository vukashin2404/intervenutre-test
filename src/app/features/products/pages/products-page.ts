import { Component, OnInit } from '@angular/core';

/* */
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';


/**** reducer ****/
import * as fromStore from '@app/features/products/store';

/***** models  ****/
import { Product } from '@app/features/products/models/product.model';


@Component({
  selector: 'app-products-page',
  template: `
    <app-spinner
        *ngIf="(isLoading$ | async); else productList">
    </app-spinner>
    <ng-template #productList>
        <app-products-list
            [products]='(productsData$ | async)'
            (handleProductDelete)="deleteProducItem($event)"
            (handleProductUpdate)="updateProducItem($event)">
        </app-products-list>
    </ng-template>
`,
  styles : [`
  :host {
      width:100%;
  }`]
})

export class ProductsPage implements OnInit  {
    public productsData$: Observable<Product[]>;
    public isLoading$: Observable<Boolean>;

    public term: String = 'like';
    public showOnlyFavourites: boolean;

    public formValue$ = new Subject();
    public showOnlyFavourites$ = new Subject();


  constructor(
    private _store: Store<fromStore.ItemsState>) {

        //
        this.formValue$.subscribe((val: any) => {
            const { price } = val;
            switch (price) {
                case 'minimum':
                    this.productsData$ = this._store.select(fromStore.getMinimumPriceProducts);
                    break;
                case 'maximum':
                    this.productsData$ = this._store.select(fromStore.getMaximumPriceProducts);
                    break;
                case 'all':
                    this.productsData$ = this._store.select(fromStore.getAllProducts);
                    break;
            }
        });
    }

    ngOnInit() {
        this.productsData$ = this._store.select(fromStore.getAllProducts);
        this.isLoading$ = this._store.select(fromStore.getProductsLoading);
        }

    updateProductList(formValue) {
        this.formValue$.next(formValue);
        }

    updateProducItem(item) {
        this._store.dispatch(new fromStore.UpdateProductActions(item));
    }

    deleteProducItem(item) {
        this._store.dispatch(new fromStore.DeleteProductActions(item));
    }





}




