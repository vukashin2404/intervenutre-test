import { Component, Input, Output, EventEmitter } from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';

/***** models  ****/
import { Product } from '@app/features/products/models/product.model';



@Component({
  selector: 'app-products-filters',
  template: `
        <h2>Filters</h2>
        <form class="example-container" [formGroup]="options">
            <div>
            <label>Float label: </label>
            <mat-radio-group formControlName="price">
                <mat-radio-button value="all">All</mat-radio-button>
                <mat-radio-button value="minimum">Maximum $50</mat-radio-button>
                <mat-radio-button value="maximum">Minimum $50</mat-radio-button>
            </mat-radio-group>
            </div>
        </form>
  `,
  styleUrls : ['products-filters.component.scss']
})

export class ProductsFiltersComponent {
    public options: FormGroup;

    @Input() products: Product[];
    @Output() handleCriteraChanges = new EventEmitter<FormGroup>();

    constructor(fb: FormBuilder) {
        this.options = fb.group({
            price: 'all',
        });

        this.onChanges();
    }

    onChanges(): void {
        this.options.valueChanges.subscribe(formValue => {
          // form has changed
          this.handleCriteraChanges.emit(formValue);
        });
      }


}

