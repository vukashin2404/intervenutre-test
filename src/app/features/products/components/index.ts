import { ProductsListComponent } from './products-list/products-list.components';
import { ProductItemComponent } from './product-item/product-item.component';
import { ProductTagsComponent } from './product-tags/product-tags.component';
import { ProductsFiltersComponent } from './products-filters/products-filters.components';
import { ProductActionLikeComponent, ProductActionsComponent } from './product-actions';


export const components: any[] = [
    ProductsListComponent,
    ProductItemComponent,
    ProductTagsComponent,
    ProductsFiltersComponent,
    ProductActionLikeComponent,
    ProductActionsComponent
  ];

export { ProductsListComponent } from './products-list/products-list.components';
export { ProductItemComponent } from './product-item/product-item.component';
export { ProductTagsComponent } from './product-tags/product-tags.component';
export { ProductsFiltersComponent } from './products-filters/products-filters.components';
export { ProductActionLikeComponent, ProductActionsComponent } from './product-actions';

