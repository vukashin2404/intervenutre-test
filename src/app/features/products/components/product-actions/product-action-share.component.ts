import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'app-product-action-share',
    template: `
    <mat-icon
        class="grey-icon"
        svgIcon="share">
    <span *ngIf="shareState && shareState > 0"
        matBadge="{{shareState}}"
        matBadgeColor="warn"
        matBadgeOverlap="false"></span>
    </mat-icon>
  `,
    styles: [`
    :host .grey-icon {
        color:#c5c5c5;
    }

    :host span {
        position:absolute;
        right:10px;
    }`]
})
export class ProductActionShareComponent {

    @Input() shareState: number;

    constructor() { }
}
