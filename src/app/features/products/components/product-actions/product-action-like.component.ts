

import { Component, Input, Output, EventEmitter } from '@angular/core';

/***** animations  ****/
import * as fromAnimations from '@app/shared/animations';


@Component({
    selector: 'app-action-like',
    template: `
        <mat-icon
            *ngIf="!likeState; else isFavourites"
            class="grey-icon"
            svgIcon="like">
        </mat-icon>
        <ng-template #isFavourites>
            <mat-icon
                [@Appear]="'appear'"
                class="red-icon"
                svgIcon="liked">
            </mat-icon>
        </ng-template>
        `,
    styles: [`
    :host .red-icon {
        fill: red;
    }
    :host .grey-icon {
        color:#c5c5c5;
    }`],
    animations: [
        fromAnimations.appear
    ]
})

export class ProductActionLikeComponent {
    @Input() likeState: Boolean;
}

