import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'app-product-actions',
    template: `
        <!-- Like -->
        <app-action-like
            [likeState]="likeState"
            (click)="productUpdate('like')">
        </app-action-like>
        <!-- Share -->
        <app-product-action-share
            [shareState]="shareState">
        </app-product-action-share>
        `,
})

export class ProductActionsComponent {
    @Input() shareState: number;
    @Input() likeState: number;

    @Output() handleDeleteProduct = new EventEmitter();
    @Output() handleUpdateProduct = new EventEmitter();

    public productUpdate(type: string) {
        this.handleUpdateProduct.emit(type);
    }
}

