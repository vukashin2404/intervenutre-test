
import { Component, Input, Output, EventEmitter } from '@angular/core';

/***** models  ****/
import { Product } from '@app/features/products/models/product.model';


@Component({
    selector: 'app-product-item',
    templateUrl: 'product-item.component.html',
    styleUrls: ['./product-item.component.scss']
})

export class ProductItemComponent {
    @Input() product: Product;

    @Output() handleProductUpdate: EventEmitter<any> = new EventEmitter();
    @Output() handleProductDelete: EventEmitter<any> = new EventEmitter();

    productUpdate(type) {
        // like
        switch (type) {
            case 'like':
                this.product.like = !this.product.like;
                break;
            case 'share':
                this.product.share++;
                break;
        }
        this.handleProductUpdate.emit(this.product);
    }

    get likeButtonText() {
        return this.product.like ? 'UNLIKE' : 'LIKE';
    }
}

