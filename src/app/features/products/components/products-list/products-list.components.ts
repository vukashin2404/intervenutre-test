import { Component, Input, Output, EventEmitter } from '@angular/core';

/***** models  ****/
import { Product } from '@app/features/products/models/product.model';

/***** animations  ****/
import * as fromAnimations from '@app/shared/animations';


@Component({
  selector: 'app-products-list',
  template: `
    <section class="flex-layout">
        <app-product-item
            *ngFor="let product of products"
            [@Leave]="'fadeOut'"
            [product]="product"
            (handleProductDelete)="productDelete($event)"
            (handleProductUpdate)="productUpdate($event)">
        </app-product-item>
    </section>
  `,
  styles : [`
  :host .flex-layout {
    display: flex;
    flex-grow: 1;
    flex-wrap: wrap;
    justify-content: space-between;

  }`],
  animations: [
    fromAnimations.fadeOut
  ]
})

export class ProductsListComponent {
  @Input() products: Product[];

  @Output() handleProductUpdate: EventEmitter<any> = new EventEmitter();
  @Output() handleProductDelete: EventEmitter<any> = new EventEmitter();

  productUpdate(item: Product) {
      this.handleProductUpdate.emit(item);
  }

  productDelete(item: Product) {
      this.handleProductDelete.emit(item);
  }

}


