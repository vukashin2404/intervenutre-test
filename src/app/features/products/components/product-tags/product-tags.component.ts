import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-product-tags',
  template: `
    <mat-chip-list>
        <mat-chip
          *ngFor="let tag of tags"
          color="primary" selected>{{ tag }}
        </mat-chip>
    </mat-chip-list>
    `,
  styles : [`
  `]
})

export class ProductTagsComponent  {
    @Input() tags: Array<string>;
}

