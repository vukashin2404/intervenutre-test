import { Action } from '@ngrx/store';
import { type } from '@app/shared/helpers/type.helpers';

/***** models  ****/
import { Product } from '@app/features/products/models/product.model';


export const ActionTypes = {
    FETCH_PRODUCT_START:                              type('[Products] Fetch Products Start'),
    FETCH_PRODUCT_SUCCEED:                            type('[Products] Fetch Products Succeed'),
    FETCH_PRODUCT_FAILED:                             type('[Products] Fetch Products Failed'),
    UPDATE_PRODUCT:                                   type('[Products] Update Product'),
    UPDATE_PRODUCT_SUCCEED:                           type('[Products] Update Product Succeed'),
    UPDATE_PRODUCT_FAILED:                            type('[Products] Update Product Failed'),
    DELETE_PRODUCT:                                   type('[Products] Delete Product'),
    DELETE_PRODUCT_SUCCEED:                           type('[Products] Delete Product Succeed'),
};

export class FetchProductsStartActions implements Action {
  type = ActionTypes.FETCH_PRODUCT_START;
  constructor(public payload?: any) { }
}


export class FetchProductsSucceedActions implements Action {
    readonly type = ActionTypes.FETCH_PRODUCT_SUCCEED;
    constructor(public payload: Product[]) { }
  }

export class FetchProductsFailedActions implements Action {
    type = ActionTypes.FETCH_PRODUCT_FAILED;
    constructor(public payload?: any) { }
}

export class UpdateProductActions implements Action {
  type = ActionTypes.UPDATE_PRODUCT;
  constructor(public payload?: any) { }
}

export class UpdateProductSucceedActions implements Action {
  type = ActionTypes.UPDATE_PRODUCT_SUCCEED;
  constructor(public payload?: any) { }
}

export class UpdateProductFailedActions implements Action {
  type = ActionTypes.UPDATE_PRODUCT_FAILED;
  constructor(public payload?: any) { }
}

export class DeleteProductActions implements Action {
  type = ActionTypes.DELETE_PRODUCT;
  constructor(public payload?: any) { }
}

export class DeleteProductSucceedActions implements Action {
  type = ActionTypes.DELETE_PRODUCT_SUCCEED;
  constructor(public payload?: any) { }
}







export type Actions
  = FetchProductsStartActions
  | FetchProductsSucceedActions
  | FetchProductsFailedActions
  | UpdateProductActions
  | UpdateProductSucceedActions
  | UpdateProductFailedActions
  | DeleteProductActions
  | DeleteProductSucceedActions;

