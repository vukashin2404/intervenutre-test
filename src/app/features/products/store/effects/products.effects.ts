import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { catchError, map, switchMap, withLatestFrom } from 'rxjs/operators';

import { of } from 'rxjs/observable/of';
import { Store } from '@ngrx/store';

/**** reducer ****/
import * as fromStoreSelector from '@app/features/products/store/selectors';
import * as fromStore from '@app/features/products/store/reducers';

/***** services  ****/
import { ProductsService } from '@app/features/products/services/products.service';
import { NotificationsService } from '@app/core/services/notifications/notifications.service';

/***** actions  ****/
import * as ProductsActions from '@app/features/products/store/actions/products.actions';
import { ActionTypes } from '@app/features/products/store/actions/products.actions';

/***** models  ****/
import { Product } from '@app/features/products/models/product.model';


@Injectable()
export class ProductsEffects {
    constructor(
        private actions$: Actions,
        private _store: Store<fromStore.ItemsState>,
        private _productsService: ProductsService,
        private _notificationsService: NotificationsService) {}


    @Effect()
    productsFetch$ = this.actions$
    .pipe(ofType(ActionTypes.FETCH_PRODUCT_START))
    .pipe(switchMap((action: ProductsActions.FetchProductsStartActions) => {

        return this._productsService
                    .list()
                    .pipe(
                        map((productsData: Product[]) =>  new ProductsActions.FetchProductsSucceedActions(productsData)
                    ),
                    catchError(error => of(new ProductsActions.FetchProductsFailedActions(error)))
                    );
            })
    );


    /* Like / Share */
    @Effect()
    productsUpdate$ = this.actions$
    .pipe(ofType(ActionTypes.UPDATE_PRODUCT))
    .pipe(switchMap((action: ProductsActions.UpdateProductActions) => {

        return this._productsService
                    .update(action.payload)
                    .pipe(
                        map((product: Product) =>  {
                            this._notificationsService.open(`${product.name} has been successfully updated`);
                            return new ProductsActions.UpdateProductSucceedActions(product);
                        }
                    ),
                    catchError(error => of(new ProductsActions.UpdateProductFailedActions(error)))
                    );
            })
    );

    @Effect()
    productsDelete$ = this.actions$
    .pipe(ofType(ActionTypes.DELETE_PRODUCT))
    .pipe(switchMap((action: ProductsActions.DeleteProductActions) => {
        const { title, id } = action.payload;

        return this._productsService
                    .delete(id)
                    .pipe(
                        map((product: Product) =>  {
                            this._notificationsService.open(`${title} has been successfully deleted`);
                            return new ProductsActions.DeleteProductSucceedActions(action.payload);
                        }
                    ),
                    catchError(error => of(new ProductsActions.UpdateProductFailedActions(error)))
                    );
            })
    );






}
