import { createSelector } from '@ngrx/store';

import * as fromRoot from '@app/store';
import * as fromFeature from '@app/features/products/store/reducers';
import * as fromProducts from '@app/features/products/store/reducers/products.reducers';

import { Product } from '@app/features/products/models/product.model';


export const getProductsState = createSelector(
  fromFeature.getItemsState,
  (state: fromFeature.ItemsState) => {

        return state.items;


  }
);

export const getProductsEntities = createSelector(
    getProductsState,
  fromProducts.getProductsEntities
);

export const getSelectedProduct = createSelector(
  getProductsEntities,
  fromRoot.getRouterState,
  (entities, router): Product => {
    return router.state && entities[router.state.params.productId];
  }
);




export const getAllProducts = createSelector(getProductsEntities, entities => {
    return Object.keys(entities).map(id => {
        return entities[id];
    });
});

export const getMaximumPriceProducts = createSelector(getProductsEntities, entities => {

    return Object.keys(entities)
        .map(id => {
            return entities[id];
        })
        .filter(entitie => {
            if (parseInt((entitie.price).replace('$', ''), 10) > 50) {
                return entitie;
            }
        });
});

export const getMinimumPriceProducts = createSelector(getProductsEntities, entities => {

    return Object.keys(entities)
        .map(id => {
            return entities[id];
        })
        .filter(entitie => {
            if (parseInt((entitie.price).replace('$', ''), 10) < 50) {
                return entitie;
            }
        });
});


export const getProductsLoaded = createSelector(
  getProductsState,
  fromProducts.getProductsLoaded
);
export const getProductsLoading = createSelector(
  getProductsState,
  fromProducts.getProductsLoading
);
