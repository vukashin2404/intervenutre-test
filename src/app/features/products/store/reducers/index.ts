
import { ActionReducerMap, createFeatureSelector } from '@ngrx/store';

import * as fromProducts from './products.reducers';

export interface ItemsState {
  items: fromProducts.ProductsState;
}

export const reducers: ActionReducerMap<ItemsState> = {
    items: fromProducts.reducer,
};

export const getItemsState = createFeatureSelector<ItemsState>(
  'products'
);


