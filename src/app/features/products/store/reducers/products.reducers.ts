import { UtilsHelpers } from '@app/shared/helpers/utils.helpers';


/***** actions  ****/
import * as ProductsActions from '@app/features/products/store/actions/products.actions';

/***** models  ****/
import {Product} from '@app/features/products/models/product.model';


export interface ProductsState {
    entities: { [id: number]: Product };
    loaded: boolean;
    loading: boolean;
    errors: any;
}

/***** initial state  ****/
export const initialState: ProductsState = {
    entities: null,
    loaded: false,
    loading: false,
    errors : null
};

/***** methods  ****/


const fetchingProductsStart = (state, action) => {
    return UtilsHelpers.prototype.updateObject(state,
      {
        loading: true,
        loaded : false,
      });
  };

const fetchingProductsSucceed = (state, action) => {
    /**use entity pattern ***/

    const entities = UtilsHelpers.prototype.flatten(action.payload);

    return UtilsHelpers.prototype.updateObject(state,
        {
            loading: false,
            loaded: true,
            errors : null,
            entities
        });
};

const fetchingProductsFailed = (state, action) => {
    const errors = action.payload;
    return UtilsHelpers.prototype.updateObject(state,
        {
            loading: false,
            loaded: false,
            errors
        });
};

const updateProductStart = (state, action) => {
    return UtilsHelpers.prototype.updateObject(state,
      { });
  };

const updateProductSucceed = (state, action) => {
    /**use entity pattern ***/

    const entities = state.entities;

    return UtilsHelpers.prototype.updateObject(state,
        {
            errors : null,
            entities
        });
};

const updateProductFailed = (state, action) => {
    const errors = action.payload;
    return UtilsHelpers.prototype.updateObject(state,
        {
            errors
        });
};

const deleteProductStart = (state, action) => {
    return UtilsHelpers.prototype.updateObject(state,
      { });
  };

const deleteProduct = (state, action) => {

        const { id } = action.payload;
        const { [id]: removed, ...entities } = state.entities;

        return UtilsHelpers.prototype.updateObject(state,
            {
                entities
            });
    };


/***** reducer  ****/
export function reducer(state = initialState, action: ProductsActions.Actions): ProductsState {
    switch (action.type) {
        case ProductsActions.ActionTypes.FETCH_PRODUCT_START: { return fetchingProductsStart(state, action); }
        case ProductsActions.ActionTypes.FETCH_PRODUCT_SUCCEED: { return fetchingProductsSucceed(state, action); }
        case ProductsActions.ActionTypes.FETCH_PRODUCT_FAILED: { return fetchingProductsFailed(state, action); }
        //
        case ProductsActions.ActionTypes.UPDATE_PRODUCT: { return updateProductStart(state, action); }
        case ProductsActions.ActionTypes.UPDATE_PRODUCT_SUCCEED: { return updateProductSucceed(state, action); }
        case ProductsActions.ActionTypes.UPDATE_PRODUCT_FAILED: { return updateProductFailed(state, action); }

        //
        case ProductsActions.ActionTypes.DELETE_PRODUCT: { return deleteProductStart(state, action); }
        case ProductsActions.ActionTypes.DELETE_PRODUCT_SUCCEED: { return deleteProduct(state, action); }

        default: return state;
    }
}

export const getProductsLoaded = (state: ProductsState) => state.loaded;
export const getProductsLoading = (state: ProductsState) => state.loading;
export const getProductsEntities = (state: ProductsState) => state.entities;




