import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';

import { Observable } from 'rxjs';
import { select, Store } from '@ngrx/store';
import { filter, take, tap } from 'rxjs/operators';

/***** models  ****/
import { Product } from '@app/features/products/models/product.model';

/**** reducer ****/
import * as fromProducts from '@app/features/products/store/reducers/products.reducers';

/**** selectors ****/
import * as fromProductsSelectors from '@app/features/products/store/selectors/products.selectors';

/**** actions ****/
import * as ProducsActions from '@app/features/products/store/actions/products.actions';


@Injectable({
  providedIn: 'root'
})

export class ProductsResolver implements Resolve<Product[]> {

  constructor(
    private _store: Store<fromProducts.ProductsState>) {}

  resolve(): Observable<Product[]> {

     // Check if products exist from the store
     return this._store.pipe(select(fromProductsSelectors.getProductsEntities))
            .pipe(
            tap((exist: any) => {
                // Check if products doesn't exist dispatch action (effect)
                if (!exist) {
                    // méthod to get throught http infos from the user
                    this._store.dispatch(new ProducsActions.FetchProductsStartActions());
                }
            }),
        filter(exist => exist),
        take(1)
        );
        }
    }

