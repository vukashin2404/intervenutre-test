/*
 Product
*/

export class Product {
    public id: any;
    public index: number;
    public isActive: boolean;
    public like: boolean;
    public share = 0;
    public price: string;
    public quantity: number;
    public picture: string;
    public name: string;
    public description: number;

    constructor(
        id: any,
        index: number,
        isActive: boolean,
        like: boolean,
        price: string,
        quantity: number,
        share: number,
        picture: string,
        name: string,
        description: number) {
        this.id = id;
        this.index = index;
        this.isActive = isActive;
        this.like = like;
        this.price = price;
        this.quantity = quantity;
        this.share = share;
        this.picture = picture;
        this.name = name;
        this.description = description;
    }
}






