import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

/*********** Import Model *********/
import { Product } from '@app/features/products/models/product.model';

/*********** Import Resource service *********/
import { ResourceService } from '@app/shared/services/resources.service';

/*********** Import CompanySerializer interface *********/
import { ProductSerializer } from '@app/features/products/interfaces/product-serializer.interface';

@Injectable({
  providedIn: 'root'
})

export class ProductsService extends ResourceService<Product> {

    searchOption = [];

    constructor(httpClient: HttpClient) {
        super(
            httpClient,
            'http://localhost:3000',
            'products',
            new ProductSerializer()
        );
    }

}


