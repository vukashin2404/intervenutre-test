import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '@app/shared/modules/material.module';

/***** modules ****/
import { SharedModule } from '@app/shared/shared.module';
import { StoreModule } from '@ngrx/store';

/*****Components****/
import * as fromComponents from './components';

/*****Pages****/
import * as fromPages from './pages';

/***** routing ****/
import { ProductsRoutingModule } from './products-routing.module';

/***** effects *****/
import { EffectsModule } from '@ngrx/effects';

/***** reducers *****/
import { reducers, effects } from '@app/features/products/store';
import { ProductActionShareComponent } from './components/product-actions/product-action-share.component';

@NgModule({
    imports: [
        CommonModule,
        MaterialModule,
        ProductsRoutingModule,
        SharedModule,
        StoreModule.forFeature('products', reducers),
        EffectsModule.forFeature(effects),
    ],
    declarations: [
        ...fromComponents.components,
        ...fromPages.pages,
        ProductActionShareComponent
    ],
    exports: [
        ...fromComponents.components,
        ...fromPages.pages
    ]
})
export class ProductsModule { }


