import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import * as fromPages from './pages';

/***** Resolver ****/
import { ProductsResolver } from '@app/features/products/resolver/product.resolver';


const routes: Routes = [
  {
    path : '',
    component : fromPages.ProductsPage,
    resolve: {
      products: ProductsResolver
    },
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductsRoutingModule { }

