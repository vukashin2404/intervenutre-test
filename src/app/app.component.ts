import { Component } from '@angular/core';

/***** services *****/
import { AppSandboxService } from '@app/core/services/sandbox/app-sandbox.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  title = 'test-angular';

  constructor(private _appSandboxService: AppSandboxService) {
    // icons
    this._appSandboxService.registerAppIcons();
  }
}
