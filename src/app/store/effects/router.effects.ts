import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

import { Actions, Effect, ofType } from '@ngrx/effects';
import * as RouterActions from '../actions';


import { map, tap } from 'rxjs/operators';


@Injectable()
export class RouterEffects {
    constructor(
        private actions$: Actions,
        private router: Router,
        private location: Location
    ) {}


    @Effect({ dispatch : false })
    navigate$ = this.actions$.pipe(ofType(RouterActions.GO))
    .pipe(
        map((action: RouterActions.Go) => action.payload),
        tap(({ path, query: queryParams, extra}) => {
            this.router.navigate(path, { queryParams, ...extra});
        })
    );

    @Effect({ dispatch : false })
    navigateBack$ = this.actions$.pipe(ofType(RouterActions.BACK))
    .pipe(
        tap(() => this.location.back())
    );

    @Effect({ dispatch : false })
    navigateForward$ = this.actions$.pipe(ofType(RouterActions.FORWARD))
    .pipe(
        tap(() => this.location.forward())
    );
}
