import { NgModule, Optional, SkipSelf } from '@angular/core';

/*****Modules****/
import { CommonModule } from '@angular/common';
import { throwIfAlreadyLoaded } from './module-import-guard';
import { RouterModule } from '@angular/router';
import { SharedModule } from '@app/shared/shared.module';
import { CoreRoutingModule } from '@app/core/core-routing.module';

/*****Components****/
import * as fromComponents from './components';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    SharedModule,
    CoreRoutingModule
  ],
  declarations: [
    ...fromComponents.components,
  ],
  exports : [
    ...fromComponents.components,
  ]
})

export class CoreModule {
  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    throwIfAlreadyLoaded(parentModule, 'CoreModule');
  }
}
