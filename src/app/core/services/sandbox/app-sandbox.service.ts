import { Injectable } from '@angular/core';

/****  ****/
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';


@Injectable({
  providedIn: 'root'
})

export class AppSandboxService {

  constructor(
    private _iconRegistry: MatIconRegistry,
    private _sanitizer: DomSanitizer ) {
  }

  /* Register app icons */
  registerAppIcons() {
    this._iconRegistry.addSvgIcon(
      'liked',
      this._sanitizer.bypassSecurityTrustResourceUrl('assets/baseline-favorite-24px.svg'));

    this._iconRegistry.addSvgIcon(
      'like',
      this._sanitizer.bypassSecurityTrustResourceUrl('assets/baseline-favorite_border-24px.svg'));

    this._iconRegistry.addSvgIcon(
      'share',
      this._sanitizer.bypassSecurityTrustResourceUrl('assets/baseline-share-24px.svg'));

    this._iconRegistry.addSvgIcon(
      'delete',
      this._sanitizer.bypassSecurityTrustResourceUrl('assets/baseline-clear-24px.svg'));
  }

}
