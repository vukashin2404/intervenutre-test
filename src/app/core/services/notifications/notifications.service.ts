import { Injectable, NgZone } from '@angular/core';
import { MatSnackBar } from '@angular/material';

@Injectable({
  providedIn: 'root'
})

export class NotificationsService {
  constructor(
    public snackBar: MatSnackBar,
    private zone: NgZone) {}

  public open(message, action = 'success', duration = 1000) {
    this.zone.run(() => {
      this.snackBar.open(message, action, { duration, verticalPosition: 'top', panelClass : action });
     });
    }
}

