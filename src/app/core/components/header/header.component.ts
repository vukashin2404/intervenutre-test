import { Component } from '@angular/core';

@Component({
selector: 'app-header',
template: `
    <mat-toolbar color="primary">
        <mat-toolbar-row>
        <span>Products Management</span>
        </mat-toolbar-row>
        <mat-toolbar-row
            class="custom-menu">
            <a routerLink="/products" routerLinkActive="active">Products</a>
        </mat-toolbar-row>
    </mat-toolbar>
    `,
    styleUrls : ['./header.component.scss']
    })

export class HeaderComponent  {

}


