# TestAngular

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.1.2.

## Development server
Run `npm run start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

# Phase 1
card :
Display missing informations of the product
* DESCRIPTION 
``` 
use the ellipsis pipe available in the shared folder and display only 70 characters
```
* NAME OF PRODUCT
* PICTURE
* PRICE
use the pipe currency EURO (should result to e.g €500) 
```
<img mat-card-image alt="Photo">
```

# Phase 2
When clicking on Like button or on the heart icon, the "Like state" will be updated.
In a first instance your mission will be to display 'UNLIKE' or 'LIKE' as button text depending on the  "Like state"
please use less code as possible.

# Phase 3
* Create a 'product-action-share.component' in features/products/components/product-actions
* Add a click event to update share state
* show or hide the badge if product has not been shared yet
*  display number of share : eg matBadge="15"

***Create and export the component, try to follow the same pattern as currently used in the app.***
_html : product-action-share_
```
<mat-icon
    class="grey-icon"
    svgIcon="share">
<span
    matBadge="0"
    matBadgeColor="warn"
    matBadgeOverlap="false"></span>
</mat-icon>

```
_styles : product-action-share_
```
styles : [`
    :host .grey-icon {
        color:#c5c5c5;
    }

    :host span {
        position:absolute;
        right:10px;
    }`]
```



